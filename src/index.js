import _ from 'lodash';
import './style.css';
import Icon from './home-page.png';

function component() {
    let element = document.createElement('div');
  
    // Lodash, currently included via a script, is required for this line to work
    element.innerHTML = _.join(['Hello', 'webpack', '2018'],' ');
    element.classList.add('hello');

    var myIcon=new Image();
    myIcon.src=Icon;

    element.appendChild(myIcon);
  
    return element;
  }
  
  document.body.appendChild(component());